var Mapn = angular.module('MAPN', ['ionic', 'ui.router', 'ngRoute', 'ngCordova', 'ngCordovaOauth', 'ngStorage', 'ngValidate', 'ngCordova', 'ionic-toast'])
var Mapn_API_URL = "http://54.200.176.218";
var TEMPLATE_LOADING = '<ion-spinner icon="spiral"></ion-spinner>';
// var Mapn_API_URL2 = "http://127.0.0.1:81";

Mapn.run(function($ionicPlatform) {
    $ionicPlatform.ready(function() {
        if (window.cordova && window.cordova.plugins.Keyboard) {
            // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
            // for form inputs)
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

            // Don't remove this line unless you know what you are doing. It stops the viewport
            // from snapping when text inputs are focused. Ionic handles this internally for
            // a much nicer keyboard experience.
            cordova.plugins.Keyboard.disableScroll(true);
        }
        if (window.StatusBar) {
            StatusBar.styleDefault();
        }
    });
})
Mapn.filter('myDateFormat', function myDateFormat($filter) {
    return function(text) {
        var tempdate = new Date(text.replace(/-/g, "/"));
        return $filter('date')(tempdate, "dd/MM/yyyy HH:mm");
    }
});
Mapn.config(function($validatorProvider, $stateProvider, $urlRouterProvider, $routeProvider, $ionicConfigProvider, $locationProvider) {
    $validatorProvider.setDefaults({
        errorPlacement: function(error, element) {}
    });
    if (localStorage.getItem('AUTH') != null) {

        $stateProvider
            .state('eventmenu', {
                url: "/app",
                abstract: true,
                templateUrl: "templates/menu.html"
            })
            .state('eventmenu.home', {
                url: "/index",
                controller: "HomeController",
                views: {
                    'menuContent': {
                        templateUrl: "templates/home.html"
                    }
                }
            })
            .state('eventmenu.place', {
                url: "/local/:local",
                cache: false,
                controller: "LocalController",
                views: {
                    'menuContent': {
                        templateUrl: "templates/local.html"
                    }
                }
            })
            .state('sharePhoto', {
                url: "/sharePhoto",
                controller: "SharePhotoController",
                templateUrl: "templates/sharePhoto.html"
            })
            .state('eventmenu.profile', {
                url: "/profile",
                cache: false,
                views: {
                    'menuContent': {
                        templateUrl: "templates/profile.html"
                    }
                }
            })
        $urlRouterProvider.otherwise("/app/index");

    } else {

        $stateProvider
            .state('eventmenu', {
                url: "/app",
                abstract: true,
                templateUrl: "templates/menu.html"
            })
            .state('eventmenu.home', {
                url: "/index",
                views: {
                    'menuContent': {
                        templateUrl: "templates/login.html"
                    }
                }
            })
            .state('eventmenu.register', {
                url: "/register",
                controller: 'RegisterController',
                views: {
                    'menuContent': {
                        templateUrl: "templates/register.html"
                    }
                }
            })
        $urlRouterProvider.otherwise("/about");
    }
    $stateProvider.state('about', {
        url: "/about",
        templateUrl: "templates/about.html"
    })

});
Mapn.factory('Camera', function($q) {

    return {
        getPicture: function(options) {
            var q = $q.defer();

            navigator.camera.getPicture(function(result) {
                q.resolve(result);
            }, function(err) {
                q.reject(err);
            }, options);

            return q.promise;
        }
    }

});

Mapn.factory('GeoService', function($ionicPlatform, $cordovaGeolocation) {

    var positionOptions = {
        timeout: 10000,
        enableHighAccuracy: true
    };

    return {
        getPosition: function() {
            return $ionicPlatform.ready()
                .then(function() {
                    return $cordovaGeolocation.getCurrentPosition(positionOptions);
                })
        }
    };

});

/*
 * CONTROLLERS
 */
Mapn.controller('IndexController', function($scope, $http, $cordovaOauth, $ionicHistory, AUTH, $ionicLoading, ionicToast) {
    $scope.validationOptions = {
        rules: {
            email: {
                required: true,
                email: true
            },
            password: {
                required: true,
                minlength: 4
            }
        },
        messages: {
            email: {
                required: "Informe seu e-mail",
                email: "Email invalido"
            },
            password: {
                required: "Informe sua senha",
                minlength: "Sua senha deve ter no minimo 4 caracteres"
            }
        }
    }

    $scope.makeLogin = function(form) {

        if (form.validate()) {
            var data = $("#loginForm").serializeArray().reduce(function(a, x) {
                a[x.name] = x.value;
                return a;
            }, {});

            $http({
                method: 'POST',
                data: {
                    email: data.email,
                    password: data.password
                },
                url: Mapn_API_URL + "/auth/login",
            }).then(function successCallback(response) {
                AUTH.store(response.data);
                return window.location.reload();
            }, function errorCallback(response) {
                ionicToast.show(response.data.error.message, 'bottom', false, 5000);
            });
        }
    }

});
Mapn.controller('SocialAuthController', function($scope, $http, $cordovaOauth, $ionicHistory, AUTH, $ionicLoading) {

    $scope.LoginwithFacebook = function() {

        // $http({
        //     method: 'GET',
        //     url: "https://graph.facebook.com/me?fields=id,name,email,link,picture.type(large)&access_token=EAATEkLDnsXQBAL6NiCoz2TCf9ZArBFnEiLENZBQXu81aqy0LEIwnDzFy9h7uK98QXeDwfd5MnoHsVi26mNWQFvtzsxE9cGjZAWumMS63XZAiFsN8tufdyvYJ8fNZAkD2d0s8SdeWPazZBZA6AZBSwyqWOtWkHZANqEYwZD",
        // }).then(function successCallback(response) {
        //     $http({
        //         method: 'POST',
        //         data: {
        //             facebook_id: response.data.id,
        //             name: response.data.name,
        //             photo: response.data.picture.data.url,
        //             // facebook_token: result.access_token,
        //             facebook_link: response.data.link,
        //             email: response.data.email,
        //         },
        //         url: Mapn_API_URL + "/auth/facebook",
        //     }).then(function successCallback(response) {
        //         AUTH.store(response.data);
        //         return window.location.reload();
        //     }, function errorCallback(response) {
        //         console.log( response.data.error.message );
        //         // return Materialize.toast(response.data.error.message, 5000, 'red');
        //     });
        // });
        // return true;

        $cordovaOauth.facebook("1342025629151604", ['email', 'publish_actions']).then(function(result) {
            $http({
                method: 'GET',
                url: "https://graph.facebook.com/me?fields=id,name,email,link,picture.type(large)&access_token=" + result.access_token,
            }).then(function successCallback(response) {
                $http({
                    method: 'POST',
                    data: {
                        facebook_id: response.data.id,
                        name: response.data.name,
                        photo: response.data.picture.data.url,
                        facebook_token: result.access_token,
                        facebook_link: response.data.link,
                        email: response.data.email,
                    },
                    url: Mapn_API_URL + "/auth/facebook",
                }).then(function successCallback(response) {
                    $ionicLoading.show({
                        template: TEMPLATE_LOADING
                    });
                    AUTH.store(response.data);
                    return window.location.reload();
                }, function errorCallback(response) {});
            }, function errorCallback(response) {});
        });
    };

});
Mapn.controller('RegisterController', ['$scope', '$http', 'AUTH', '$ionicLoading', '$state', 'ionicToast', function($scope, $http, AUTH, $ionicLoading, $state, ionicToast) {
    $scope.validationOptions = {
        rules: {
            email: {
                required: true,
                email: true
            },
            name: {
                required: true,
            },
            password: {
                required: true,
                minlength: 4
            }
        },
        messages: {
            email: {
                required: "Informe seu e-mail",
                email: "Email invalido"
            },
            name: {
                required: "Informe seu nome",
            },
            password: {
                required: "Informe sua senha",
                minlength: "Sua senha deve ter no minimo 4 caracteres"
            }
        }
    }

    $scope.makeRegister = function(form) {
        if (form.validate()) {
            $ionicLoading.show({
                template: TEMPLATE_LOADING
            });
            var data = $("#registerForm").serializeArray().reduce(function(a, x) {
                a[x.name] = x.value;
                return a;
            }, {});
            $http({
                method: 'POST',
                data: {
                    email: data.email,
                    name: data.name,
                    password: data.password
                },
                url: Mapn_API_URL + "/auth/register",
            }).then(function successCallback(response) {
                AUTH.store(response.data);
                $ionicLoading.hide();
                return window.location.reload();
            }, function errorCallback(response) {
                $ionicLoading.hide();
                ionicToast.show(response.data.error.message, 'bottom', false, 5000);
            });
        }
    }

}]);
Mapn.service('AUTH', [function($scope, $localStorage, $sessionStorage) {
    return {
        isAuthenticated: function() {
            return !(localStorage.getItem('AUTH') == null);
        },
        get: function() {
            return (this.isAuthenticated()) ? JSON.parse(localStorage.getItem('AUTH')) : {};
        },
        store: function(data) {
            return localStorage.setItem('AUTH', JSON.stringify(data));
        },
        loading: function(data) {
            return localStorage.setItem('AUTH', JSON.stringify(data));
        },
        logout: function(data) {
            return localStorage.removeItem("AUTH");
        },
        ban: function() {
            if (this.isAuthenticated()) return window.location.replace('/#/home');
            return false;
        }
    };
}]);
Mapn.factory('CameraService', [function($scope, $cordovaCamera) {

    return {
        takeImage: function() {
            document.addEventListener("deviceready", function() {

                var options = {
                    quality: 90,
                    destinationType: Camera.DestinationType.DATA_URL,
                    sourceType: Camera.PictureSourceType.CAMERA,
                    allowEdit: true,
                    encodingType: Camera.EncodingType.JPEG,
                    targetWidth: 600,
                    targetHeight: 600,
                    popoverOptions: CameraPopoverOptions,
                    saveToPhotoAlbum: true,
                    correctOrientation: true
                };

                $cordovaCamera.getPidscture(options).then(function(imageData) {
                    localStorage.setItem('LastImg', "data:image/jpeg;base64," + imageData);
                    return window.location.href = '#/sharePhoto';
                }, function(err) {
                    // error
                });
            }, false);

        }
    };

}]);
Mapn.controller('AplicationController', ['$scope', 'AUTH', '$ionicLoading', function($scope, AUTH, $ionicLoading) {
    $scope.isAuth = function() {
        return AUTH.isAuthenticated();
    }
    $scope.User = AUTH.get();
    $scope.logout = function() {
        $ionicLoading.show({
            template: TEMPLATE_LOADING
        });
        AUTH.logout();
        return window.location.reload();
    }
    $scope.searchPlace = function(value) {
        // if( value.length < 3)
        //    console.log( value.length );
        //    if( )
    }
}]);
Mapn.controller('AboutController', ['$scope', 'AUTH', function($scope, AUTH) {

    $scope.options = {
        // loop: true,
        effect: 'fade',
        speed: 500,
    }

    $scope.$on("$ionicSlides.sliderInitialized", function(event, data) {
        // data.slider is the instance of Swiper
        $scope.slider = data.slider;
    });

    $scope.$on("$ionicSlides.slideChangeStart", function(event, data) {
        console.log('Slide change is beginning');
    });

    $scope.$on("$ionicSlides.slideChangeEnd", function(event, data) {
        // note: the indexes are 0-based
        $scope.activeIndex = data.slider.activeIndex;
        $scope.previousIndex = data.slider.previousIndex;
    });

}]);
Mapn.controller('TesteController', ['$scope', 'AUTH', function($scope, AUTH) {
    console.log(" TesteController")
}]);
Mapn.controller('HomeController', ['$scope', '$http', 'AUTH', 'GeoService', '$ionicLoading', '$ionicModal', 'ionicToast', function($scope, $http, AUTH, GeoService, $ionicLoading, $ionicModal, ionicToast) {
    GeoService.getPosition()
        .then(function(position) {
            return localStorage.setItem('MyLocation', JSON.stringify(position.coords));
        }, function(err) {
            return localStorage.setItem('MyLocation', JSON.stringify({
                latitude: "-23.535485",
                longitude: "-46.421291"
            }));
        });
    $scope.TESTE = JSON.parse(localStorage.getItem('MyLocation'));
    $scope.CHANGE = function() {
        $scope.getLocals($scope.distanceVal);
    }
    $scope.getLocals = function(distance) {
        localStorage.setItem('DISTANCE', distance);
        $ionicLoading.show({
            template: TEMPLATE_LOADING
        });
        var Mapn_API_URL_LOCATION = Mapn_API_URL + "/places/user/" + AUTH.get()._id + "/" + $scope.TESTE.latitude + "/" + $scope.TESTE.longitude + "/" + distance + "/100";
        console.log( Mapn_API_URL_LOCATION );
        $http({
            method: 'get',
            url: Mapn_API_URL_LOCATION,
        }).then(function successCallback(response) {
            $scope.ResponseData = response.data;
            $ionicLoading.hide();
            return true;
        }, function errorCallback(response) {});
    }
    $scope.getLocals(90);
    $scope.favorite = function(id) {
        $ionicLoading.show({
            template: TEMPLATE_LOADING
        });
        $http({
            method: 'POST',
            data: {
                place_id: id,
                user_id: AUTH.get()._id,
            },
            url: Mapn_API_URL + "/places/favorite",
        }).then(function successCallback(response) {
            $scope.getLocals(localStorage.getItem('DISTANCE'));
            ionicToast.show('Favoritado', 'bottom', false, 5000);
        }, function errorCallback(response) {});
    }
}]);
Mapn.controller('CameraController', ['$scope', '$http', 'AUTH', '$cordovaCamera', '$cordovaSocialSharing', '$rootScope', '$ionicPopup', 'ionicToast',
    function($scope, $http, AUTH, $cordovaCamera, $cordovaSocialSharing, $rootScope, $ionicPopup, ionicToast) {
        $scope.insertCheckIn = function(place_id, code, redirect) {
            $scope.data = {}
            var myPopup = $ionicPopup.show({
                template: '<input type="password" ng-model="data.wifi">',
                title: 'Faça o CheckIn',
                subTitle: 'Por favor, informe o código do estabelecimento',
                scope: $scope,
                buttons: [{
                    text: 'Cancelar'
                }, {
                    text: '<b>Salvar</b>',
                    type: 'button-positive',
                    onTap: function(e) {
                        if (!$scope.data.wifi) {
                            e.preventDefault();
                        } else {
                            if (code != $scope.data.wifi) return e.preventDefault();
                            $http({
                                method: 'POST',
                                data: {
                                    "checkIn": place_id,
                                    "user_id": AUTH.get()._id,
                                    "point": "1",
                                    "code": $scope.data.wifi
                                },
                                url: Mapn_API_URL + "/checkin",
                            }).then(function successCallback(response) {
                                myPopup.close();
                                $rootScope.tab = 'items';
                                ionicToast.show('CheckIn realizado ;)', 'bottom', false, 5000);
                                if (redirect) return window.location.href = '#/app/local/' + place_id;
                                $scope.getLocal();
                                return true;
                            }, function errorCallback(response) {
                                return e.preventDefault();
                            });
                        }
                    }
                }, ]
            });
        };

        $scope.checkInCamera = function(place_id, code) {
            $scope.data = {}
            var myPopup = $ionicPopup.show({
                template: '<input type="password" ng-model="data.wifi">',
                title: 'Código do estabelecimento',
                subTitle: 'Por favor, informe o código do estabelecimento',
                scope: $scope,
                buttons: [{
                    text: 'Cancelar'
                }, {
                    text: '<b>Salvar</b>',
                    type: 'button-positive',
                    onTap: function(e) {
                        if (!$scope.data.wifi) {
                            e.preventDefault();
                        } else {
                            if (code != $scope.data.wifi) return e.preventDefault();
                            return $scope.takeImage(place_id);
                        }
                    }
                }, ]
            });
        };


        $scope.takeImage = function(place_id) {
            if (isEmpty(localStorage.getItem('checkIn'))) localStorage.setItem('checkIn', place_id);
            console.log(localStorage.getItem('checkIn'));
            document.addEventListener("deviceready", function() {

                var options = {
                    quality: 90,
                    destinationType: Camera.DestinationType.DATA_URL,
                    sourceType: Camera.PictureSourceType.CAMERA,
                    allowEdit: true,
                    encodingType: Camera.EncodingType.JPEG,
                    targetWidth: 600,
                    targetHeight: 600,
                    popoverOptions: CameraPopoverOptions,
                    saveToPhotoAlbum: true,
                    correctOrientation: true
                };

                $cordovaCamera.getPicture(options).then(function(imageData) {
                    localStorage.setItem('LastImg', "data:image/jpeg;base64," + imageData);
                    return window.location.href = '#/sharePhoto';
                }, function(err) {
                    // error
                });
            }, false);

        }

        $scope.Options = function(place_id, code, redirect, count) {
                                    console.log( place_id, code, redirect, count );
            $ionicPopup.show({
                cssClass: 'popup-vertical-buttons',
                template: "Escolha uma das ações",
                title: 'Eu quero',
                scope: $scope,
                buttons: [{
                    text: '<i class="fa fa-map-marker"></i> CheckIn',
                    type: 'button-positive',
                    onTap: function() {
                        $scope.insertCheckIn(place_id, code, redirect);
                    }
                }, {
                    text: '<i class="fa fa-camera"></i> Foto',
                    type: 'button-positive',
                    onTap: function() {

                        if( count >= 2)
                        $ionicPopup.show({
                            cssClass: 'popup-vertical-buttons',
                            title: 'Limite de fotos atingido',
                            scope: $scope,
                            buttons: [{
                                text: 'Fechar'
                            }, ]
                        });

                        else
                         $scope.checkInCamera(place_id, code );


                    }
                }, {
                    text: 'Cancelar'
                }, ]
            });
        }
    }
]);
Mapn.controller('SharePhotoController', ['$scope', '$http', 'AUTH', '$ionicLoading', '$cordovaOauth', '$rootScope', 'ionicToast', function($scope, $http, AUTH, $ionicLoading, $cordovaOauth, $rootScope, ionicToast) {
    $scope.HaveFacebook = !isEmpty(AUTH.get().facebook_id);
    // localStorage.setItem('LastImg', '')
    $scope.imageShare = localStorage.getItem('LastImg');
    $scope.cancel = function() {
        return window.location.href = '#/';
    }
    $scope.submitShare = function() {
        $ionicLoading.show({
            template: TEMPLATE_LOADING
        });
        var data = $("#socialShareForm").serializeArray().reduce(function(a, x) {
            a[x.name] = x.value;
            return a;
        }, {});

        var send = mergeObj(
            data, {
                "user_id": AUTH.get()._id,
                "checkIn": localStorage.getItem('checkIn'),
                "photo": localStorage.getItem('LastImg'),
            })
        $http({
            method: 'POST',
            data: send,
            url: Mapn_API_URL + "/social/share",
        }).then(function successCallback(response) {
            $ionicLoading.hide();
            $rootScope.tab = 'items';
            ionicToast.show('CheckIn realizado ;)', 'bottom', false, 5000);
            return window.location.href = '#/app/local/' + localStorage.getItem('checkIn');
        }, function errorCallback(response) {
            $ionicLoading.hide();
            return true;
        });
    }

    $scope.checkFacebook = function() {
        $ionicLoading.show({
            template: TEMPLATE_LOADING
        });
        if (!isEmpty(AUTH.get().facebook_token)) return $ionicLoading.hide();

        $cordovaOauth.facebook("1342025629151604", ['email', 'publish_actions']).then(function(result) {
            $http({
                method: 'GET',
                url: "https://graph.facebook.com/me?fields=id,name,email,link,picture.type(large)&access_token=" + result.access_token,
            }).then(function successCallback(response) {
                $http({
                    method: 'POST',
                    data: {
                        facebook_id: response.data.id,
                        name: response.data.name,
                        photo: response.data.picture.data.url,
                        facebook_token: result.access_token,
                        facebook_link: response.data.link,
                        email: response.data.email,
                    },
                    url: Mapn_API_URL + "/auth/facebook",
                }).then(function successCallback(response) {
                    $ionicLoading.show({
                        template: TEMPLATE_LOADING
                    });
                    AUTH.store(response.data);
                    return window.location.reload();
                }, function errorCallback(response) {});
            }, function errorCallback(response) {});
        });
    }
}]);
Mapn.controller('LocalController', ['$scope', '$http', '$stateParams', 'AUTH', '$ionicLoading', '$ionicPopup', '$rootScope', 'ionicToast', function($scope, $http, $stateParams, AUTH, $ionicLoading, $ionicPopup, $rootScope, ionicToast) {
    $scope.orderByFunction = function(friend) {
        return parseInt(friend.pointing);
    };
    if (!($rootScope.tab)) $rootScope.tab = 'details';

    $scope.getLocal = function() {
        $ionicLoading.show({
            template: TEMPLATE_LOADING
        });
        console.log( Mapn_API_URL + "/places/" + $stateParams.local + "/user/" + AUTH.get()._id );
        $http({
            method: 'get',
            url: Mapn_API_URL + "/places/" + $stateParams.local + "/user/" + AUTH.get()._id,
        }).then(function successCallback(response) {
            $scope.ResponseData = response.data;
            loadMap(response.data.location.latitude, response.data.location.longitude, "#googleMap");
            $ionicLoading.hide();
            return true;
        }, function errorCallback(response) {});
    }
    $scope.getLocal();
    localStorage.setItem('checkIn', $stateParams.local);
    $scope.local = $stateParams.local;
    $scope.place_id = $stateParams.local;
    $scope.TESTE = function() {
        $ionicPopup.show({
            cssClass: 'popup-vertical-buttons',
            template: "Escolha uma das ações",
            title: 'Eu quero',
            scope: $scope,
            buttons: [{
                text: '<b>Fazer checkIn</b>',
                type: 'button-positive',
                onTap: function() {
                    $scope.insertCheckIn();
                }
            }, {
                text: '<b>Dar feedback</b>',
                type: 'button-positive',
                onTap: function() {
                    if (!$scope.ResponseData.FeedbacksUser) return $scope.insertFeedback();
                }
            }, {
                text: 'Cancelar'
            }, ]
        });
    }
    $scope.modalItem = function(item, name, point) {
        $scope.data = {};
        var myPopup = $ionicPopup.show({
            template: '<input type="password" ng-model="data.wifi">',
            title: 'Resgatar Item',
            subTitle: 'Por favor, informe o código',
            scope: $scope,
            buttons: [{
                text: 'Cancelar'
            }, {
                text: '<b>Resgatar</b>',
                type: 'button-positive',
                onTap: function(e) {
                    if (!$scope.data.wifi) {
                        e.preventDefault();
                    } else {
                        if ($scope.ResponseData.code != $scope.data.wifi) return e.preventDefault();
                        $http({
                            method: 'POST',
                            data: {
                                item_id: item,
                                place_id: $stateParams.local,
                                point: point,
                            },
                            url: Mapn_API_URL + "/user/" + AUTH.get()._id + "/item",
                        }).then(function successCallback(response) {
                            ionicToast.show('Recompensa resgatada! :D', 'bottom', false, 5000);
                            return window.location.href = '#/app/profile';
                        }, function errorCallback(response) {
                            console.log(response.data);
                        });

                    }
                }
            }, ]
        });

    }

    $scope.modalItemHide = function() {
        $scope.ModalItem.hide();
    }

    $scope.insertCheckIn = function() {
        $scope.data = {}
        var myPopup = $ionicPopup.show({
            template: '<input type="password" ng-model="data.wifi">',
            title: ' Faça o CheckIn',
            subTitle: 'Por favor, informe o código',
            scope: $scope,
            buttons: [{
                text: 'Cancelar'
            }, {
                text: '<b>Salvar</b>',
                type: 'button-positive',
                onTap: function(e) {
                    if (!$scope.data.wifi) {
                        e.preventDefault();
                    } else {
                        if ($scope.ResponseData.code != $scope.data.wifi) return e.preventDefault();
                        $http({
                            method: 'POST',
                            data: {
                                "checkIn": $stateParams.local,
                                "user_id": AUTH.get()._id,
                                "point": "1",
                                "code": $scope.data.wifi
                            },
                            url: Mapn_API_URL + "/checkin",
                        }).then(function successCallback(response) {
                            $scope.getLocal();
                            $rootScope.tab = 'items';
                            ionicToast.show('CheckIn realizado! ;)', 'bottom', false, 5000);
                        }, function errorCallback(response) {
                            return e.preventDefault();
                        });
                    }
                }
            }, ]
        });
    };

    $scope.insertFeedback = function() {
        $scope.data = {};
        $ionicPopup.show({
            template: '<textarea placeholder="Texto" ng-model="data.feedback"></textarea>',
            title: ' Faça o feedback',
            subTitle: 'Por favor, descreva o seu feedback',
            scope: $scope,
            buttons: [{
                text: 'Cancelar'
            }, {
                text: '<b>Salvar</b>',
                type: 'button-positive',
                onTap: function(e) {
                    if (!$scope.data.feedback) {
                        e.preventDefault();
                    } else {
                        $http({
                            method: 'POST',
                            data: {
                                "place_id": $stateParams.local,
                                "user_id": AUTH.get()._id,
                                "name": "POP-UP",
                                "text": $scope.data.feedback
                            },
                            url: Mapn_API_URL + "/feedbacks",
                        }).then(function successCallback(response) {
                            $scope.getLocal();
                            $rootScope.tab = 'feedbacks';
                            ionicToast.show('Feedback criado! ;)', 'bottom', false, 5000);
                        }, function errorCallback(response) {
                            return e.preventDefault();
                        });
                    }
                }
            }, ]
        });
    }

}]);
Mapn.controller('LoginController', ['$scope', 'AUTH', function($scope, AUTH) {
    console.log(AUTH.get());
}])

Mapn.controller('ProfileController', ['$scope', '$http', 'AUTH', '$ionicLoading', function($scope, $http, AUTH, $ionicLoading) {
    $ionicLoading.show({
        template: TEMPLATE_LOADING
    });
    console.log( Mapn_API_URL + "/user/" + AUTH.get()._id );
    $http({
        method: 'get',
        url: Mapn_API_URL + "/user/" + AUTH.get()._id,
    }).then(function successCallback(response) {
        $scope.ResponseData = response.data;
        $ionicLoading.hide();
    }, function errorCallback(response) {});
    $scope.User = AUTH.get();
}])

function isEmpty(str) {
    return (!str || 0 === str.length || str == null);
}

function loadMap(latitude, longitude, divId) {
    new GMaps({
        div: divId,
        lat: latitude,
        lng: longitude
    });
}

function dataURItoBlob(dataURI) {
    var byteString = atob(dataURI.split(',')[1]);
    var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0]

    var ab = new ArrayBuffer(byteString.length);
    var ia = new Uint8Array(ab);
    for (var i = 0; i < byteString.length; i++) {
        ia[i] = byteString.charCodeAt(i);
    }

    var bb = new Blob([ab], {
        "type": mimeString
    });
    return bb;
}

function dataURItoBlob2(dataURI) {
    var byteString = atob(dataURI.split(',')[1]);
    var ab = new ArrayBuffer(byteString.length);
    var ia = new Uint8Array(ab);
    for (var i = 0; i < byteString.length; i++) {
        ia[i] = byteString.charCodeAt(i);
    }
    return new Blob([ab], {
        type: 'image/png'
    });
}

function mergeObj(obj, src) {
    for (var key in src) {
        if (src.hasOwnProperty(key)) obj[key] = src[key];
    }
    return obj;
}

function formatDate(date) {
    var monthNames = [
        "January", "February", "March",
        "April", "May", "June", "July",
        "August", "September", "October",
        "November", "December"
    ];

    var day = date.getDate();
    var monthIndex = date.getMonth();
    var year = date.getFullYear();

    return day + ' ' + monthNames[monthIndex] + ' ' + year;
}